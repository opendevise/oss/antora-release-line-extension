/* eslint-env mocha */
'use strict'

const { expect, spy, ContentCatalog } = require('./harness')

describe('release line extension', () => {
  const ext = require('@opendevise/antora-release-line-extension')

  const createGeneratorContext = () => ({
    $variables: {},
    once (eventName, fn) {
      this[eventName] = fn
    },
    require: (request) => require(request),
    getVariables () {
      return this.$variables
    },
    updateVariables (vars) {
      if ('contentAggregate' in vars) {
        const { contentAggregate } = vars
        delete vars.contentAggregate
        vars.contentCatalog = createContentCatalog(contentAggregate)
      }
      Object.assign(this.$variables, vars)
    },
  })

  const createContentCatalog = (contentAggregate, latestVersionSegment, callComplete) => {
    const playbook = { urls: { latestVersionSegment, latestVersionSegmentStrategy: 'redirect:to' } }
    return Array(
      contentAggregate.reduce((contentCatalog, { name, version, ...data }) => {
        contentCatalog.registerComponentVersion(name, version, data)
        return contentCatalog
      }, new ContentCatalog(playbook))
    ).map((contentCatalog) => {
      const $complete = () => {
        contentCatalog.getComponents().forEach((component) => {
          component.versions.forEach(({ name, version }) => {
            contentCatalog.addFile({
              path: 'modules/ROOT/pages/index.adoc',
              src: { component: name, version, module: 'ROOT', family: 'page', relative: 'index.adoc' },
            })
            contentCatalog.registerComponentVersionStartPage(name, version)
          })
        })
      }
      callComplete ? $complete() : Object.assign(contentCatalog, { $complete })
      return contentCatalog
    })[0]
  }

  describe('bootstrap', () => {
    it('should be able to require extension', () => {
      expect(ext).to.be.instanceOf(Object)
      expect(ext.register).to.be.instanceOf(Function)
    })

    it('should be able to call register function exported by extension', () => {
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext, {})
      expect(generatorContext.componentsRegistered).to.be.instanceOf(Function)
      expect(generatorContext.contentClassified).to.be.instanceOf(Function)
    })
  })

  describe('map release lines (componentsRegistered listener)', () => {
    it('should assign latestPerReleaseLineByComponent context variable even if content aggregate is empty', () => {
      const contentAggregate = []
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      expect(generatorContext.getVariables()).to.have.property('latestPerReleaseLineByComponent')
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent).to.be.instanceOf(Map)
      expect(latestPerReleaseLineByComponent).to.be.empty()
    })

    it('should use component names as keys in latestPerReleaseLineByComponent context variable', () => {
      const contentAggregate = [{ name: 'the-component', version: '5.7.0' }]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      expect(generatorContext.getVariables()).to.have.property('latestPerReleaseLineByComponent')
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent).to.be.instanceOf(Map)
      expect(latestPerReleaseLineByComponent).to.have.key('the-component')
      expect(latestPerReleaseLineByComponent.get('the-component')).to.be.instanceOf(Map)
    })

    it('should identify major version', () => {
      const contentAggregate = [{ name: 'the-component', version: '5.7.0' }]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      const latestPerReleaseLineForTheComponent = latestPerReleaseLineByComponent.get('the-component')
      expect(latestPerReleaseLineForTheComponent).to.have.any.keys('5')
      const major = latestPerReleaseLineForTheComponent.get('5')
      expect(major).to.eql({ level: 'major', version: '5.7.0' })
    })

    it('should identify minor version', () => {
      const contentAggregate = [{ name: 'the-component', version: '5.7.0' }]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      const latestPerReleaseLineForTheComponent = latestPerReleaseLineByComponent.get('the-component')
      expect(latestPerReleaseLineForTheComponent).to.have.any.keys('5.7')
      const minor = latestPerReleaseLineForTheComponent.get('5.7')
      expect(minor).to.eql({ level: 'minor', version: '5.7.0' })
    })

    it('should map major and minor to greatest patch version', () => {
      const contentAggregate = [
        { name: 'the-component', version: '5.7.1' },
        { name: 'the-component', version: '5.7.0' },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component').get('5')).to.have.property('version', '5.7.1')
      expect(latestPerReleaseLineByComponent.get('the-component').get('5.7')).to.have.property('version', '5.7.1')
    })

    it('should map major and minor to greatest patch version that is not a prerelease', () => {
      const contentAggregate = [
        { name: 'the-component', version: '5.7.0' },
        { name: 'the-component', version: '5.7.1' },
        { name: 'the-component', version: '5.7.2', prerelease: true },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component').get('5')).to.have.property('version', '5.7.1')
      expect(latestPerReleaseLineByComponent.get('the-component').get('5.7')).to.have.property('version', '5.7.1')
    })

    it('should skip prerelease version that has prerelease segment', () => {
      const contentAggregate = [
        { name: 'the-component', version: '5.7.0' },
        { name: 'the-component', version: '5.7.1' },
        { name: 'the-component', version: '5.7.1-rc.1', prerelease: true },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component').get('5')).to.have.property('version', '5.7.1')
      expect(latestPerReleaseLineByComponent.get('the-component').get('5.7')).to.have.property('version', '5.7.1')
    })

    it('should identify greatest major and minor by first sorting copy of content aggregate', () => {
      const contentAggregate = [
        { name: 'the-component', version: '5.7.0' },
        { name: 'the-component', version: '6.0.2' },
        { name: 'the-component', version: '5.7.2' },
        { name: 'the-component', version: '5.7.1' },
        { name: 'the-component', version: '5.8.11' },
        { name: 'the-component', version: '5.8.12' },
        { name: 'the-component', version: '6.0.1' },
        { name: 'the-component', version: '5.8.10' },
        { name: 'the-component', version: '6.0.0' },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      expect(contentAggregate[0]).to.have.property('version', '5.7.0')
      expect(contentAggregate[contentAggregate.length - 1]).to.have.property('version', '6.0.0')
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      const latestPerReleaseLineForTheComponent = latestPerReleaseLineByComponent.get('the-component')
      const actual = [
        ['6', '6.0.2'],
        ['6.0', '6.0.2'],
        ['5', '5.8.12'],
        ['5.8', '5.8.12'],
        ['5.7', '5.7.2'],
      ]
      actual.forEach(([abbrVersion, actualVersion]) => {
        expect(latestPerReleaseLineForTheComponent.get(abbrVersion)).to.have.property('version', actualVersion)
      })
      const keys = [...latestPerReleaseLineForTheComponent.keys()]
      expect(keys).to.have.ordered.members(actual.map((it) => it[0]))
    })

    it('should map major and minor to prerelease if no release', () => {
      const contentAggregate = [{ name: 'the-component', version: '5.7.0', prerelease: true }]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component').get('5')).to.have.property('version', '5.7.0')
      expect(latestPerReleaseLineByComponent.get('the-component').get('5.7')).to.have.property('version', '5.7.0')
    })

    it('should map major and minor to greatest prerelease if no release', () => {
      const contentAggregate = [
        { name: 'the-component', version: '5.7.0-M1', prerelease: true },
        { name: 'the-component', version: '5.7.0', prerelease: '-SNAPSHOT' },
        { name: 'the-component', version: '5.7.0-RC1', prerelease: true },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component').get('5')).to.have.property('version', '5.7.0')
      expect(latestPerReleaseLineByComponent.get('the-component').get('5.7')).to.have.property('version', '5.7.0')
    })

    it('should map major to greatest non-prerelease', () => {
      const contentAggregate = [
        { name: 'the-component', version: '5.7.0' },
        { name: 'the-component', version: '5.7.1' },
        { name: 'the-component', version: '5.8.0', prerelease: true },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component').get('5')).to.have.property('version', '5.7.1')
    })

    it('should assign versionSegment property for each latest minor', () => {
      const contentAggregate = [
        { name: 'the-component', version: '5.6.2' },
        { name: 'the-component', version: '5.7.0' },
        { name: 'the-component', version: '5.7.1' },
        { name: 'the-component', version: '5.6.3' },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { contentCatalog } = generatorContext.getVariables()
      expect(contentCatalog.getComponentVersion('the-component', '5.7.1')).to.have.property('versionSegment', '5.7')
      expect(contentCatalog.getComponentVersion('the-component', '5.7.0')).to.not.have.property('versionSegment')
      expect(contentCatalog.getComponentVersion('the-component', '5.6.3')).to.have.property('versionSegment', '5.6')
      expect(contentCatalog.getComponentVersion('the-component', '5.6.2')).to.not.have.property('versionSegment')
    })

    it('should create empty map if only version does not follow SemVer', () => {
      const contentAggregate = [{ name: 'the-component', version: 'the-version' }]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component')).to.be.empty()
    })

    it('should skip any version which does not follow SemVer', () => {
      const contentAggregate = [
        { name: 'the-component', version: '6.0.1' },
        { name: 'the-component', version: 'the-version' },
        { name: 'the-component', version: '6.0.0' },
        { name: 'the-component', version: 'latest' },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent.get('the-component').get('6')).to.have.property('version', '6.0.1')
      expect(latestPerReleaseLineByComponent.get('the-component').get('6.0')).to.have.property('version', '6.0.1')
    })

    it('should map major and minor for each component', () => {
      const contentAggregate = [
        { name: 'component-a', version: '1.0.0' },
        { name: 'component-a', version: '1.0.1' },
        { name: 'component-b', version: '2.1.0' },
        { name: 'component-b', version: '2.1.1' },
      ]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentAggregate })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      const { latestPerReleaseLineByComponent } = generatorContext.getVariables()
      expect(latestPerReleaseLineByComponent).to.have.keys('component-a', 'component-b')
      expect(latestPerReleaseLineByComponent.get('component-a').get('1')).to.have.property('version', '1.0.1')
      expect(latestPerReleaseLineByComponent.get('component-a').get('1.0')).to.have.property('version', '1.0.1')
      expect(latestPerReleaseLineByComponent.get('component-b').get('2')).to.have.property('version', '2.1.1')
      expect(latestPerReleaseLineByComponent.get('component-b').get('2.1')).to.have.property('version', '2.1.1')
    })
  })

  describe('add splat aliases for release lines (contentClassified listener)', () => {
    it('should not add splat aliases if release line data is empty', () => {
      const contentAggregate = [{ name: 'the-component', version: '4.2.3' }]
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      const contentCatalog = createContentCatalog(contentAggregate, undefined, true)
      generatorContext.updateVariables({ contentCatalog, latestPerReleaseLineByComponent: new Map() })
      generatorContext.contentClassified(generatorContext.getVariables())
      expect(contentCatalog.findBy({ family: 'alias' })).to.be.empty()
    })

    it('should not add splat aliases if release line data for component is empty', () => {
      const contentAggregate = [{ name: 'the-component', version: '4.2.3' }]
      const latestPerReleaseLineByComponent = new Map([['the-component', new Map()]])
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      const contentCatalog = createContentCatalog(contentAggregate, undefined, true)
      generatorContext.updateVariables({ contentCatalog, latestPerReleaseLineByComponent })
      generatorContext.contentClassified(generatorContext.getVariables())
      expect(contentCatalog.findBy({ family: 'alias' })).to.be.empty()
    })

    it('should add splat alias for major version to active version', () => {
      const contentAggregate = [
        { name: 'the-component', version: '4.2.3', versionSegment: '4.2' },
        { name: 'the-component', version: '4.2.2' },
      ]
      const latestPerReleaseLineByComponent = new Map([
        [
          'the-component',
          new Map([
            ['4', { level: 'major', version: '4.2.3' }],
            ['4.2', { level: 'minor', version: '4.2.3' }],
          ]),
        ],
      ])
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      const contentCatalog = createContentCatalog(contentAggregate, undefined, true)
      generatorContext.updateVariables({ contentCatalog, latestPerReleaseLineByComponent })
      generatorContext.contentClassified(generatorContext.getVariables())
      const actual = contentCatalog.findBy({ family: 'alias' }).find(({ pub }) => pub.url === '/the-component/4')
      expect(actual).to.exist()
      const expected = {
        pub: { url: '/the-component/4', splat: true },
        src: { component: 'the-component', version: '4', module: 'ROOT', family: 'alias', relative: '' },
        rel: {
          pub: { url: '/the-component/4.2', splat: true },
          src: { component: 'the-component', version: '4.2.3', module: 'ROOT', family: 'alias', relative: '' },
        },
      }
      expect(actual.src).to.include(expected.src)
      expect(actual.pub).to.include(expected.pub)
      expect(actual.rel.pub).to.include(expected.rel.pub)
      expect(actual.rel.src).to.include(expected.rel.src)
    })

    it('should add splat alias for actual version to active version', () => {
      const contentAggregate = [
        { name: 'the-component', version: '4.2.3', versionSegment: '4.2' },
        { name: 'the-component', version: '4.2.2' },
      ]
      const latestPerReleaseLineByComponent = new Map([
        [
          'the-component',
          new Map([
            ['4', { level: 'major', version: '4.2.3' }],
            ['4.2', { level: 'minor', version: '4.2.3' }],
          ]),
        ],
      ])
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      const contentCatalog = createContentCatalog(contentAggregate, undefined, true)
      generatorContext.updateVariables({ contentCatalog, latestPerReleaseLineByComponent })
      generatorContext.contentClassified(generatorContext.getVariables())
      const actual = contentCatalog.findBy({ family: 'alias' }).find(({ pub }) => pub.url === '/the-component/4.2.3')
      expect(actual).to.exist()
      const expected = {
        pub: { url: '/the-component/4.2.3', splat: true },
        src: { component: 'the-component', version: '4.2.3', module: 'ROOT', family: 'alias', relative: '' },
        rel: {
          pub: { url: '/the-component/4.2', splat: true },
          src: { component: 'the-component', version: '4.2.3', module: 'ROOT', family: 'alias', relative: '' },
        },
      }
      expect(actual.src).to.include(expected.src)
      expect(actual.pub).to.include(expected.pub)
      expect(actual.rel.pub).to.include(expected.rel.pub)
      expect(actual.rel.src).to.include(expected.rel.src)
    })

    it('should not add splat aliases that conflict with splat alias for latest version segment', () => {
      const contentAggregate = [
        { name: 'the-component', version: '4.2.3', versionSegment: '4.2' },
        { name: 'the-component', version: '4.2.2' },
      ]
      const latestPerReleaseLineByComponent = new Map([
        [
          'the-component',
          new Map([
            ['4', { level: 'major', version: '4.2.3' }],
            ['4.2', { level: 'minor', version: '4.2.3' }],
          ]),
        ],
      ])
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      const contentCatalog = createContentCatalog(contentAggregate, 'latest', true)
      const removeFile = (contentCatalog.removeFile = spy(contentCatalog.removeFile))
      generatorContext.updateVariables({ contentCatalog, latestPerReleaseLineByComponent })
      generatorContext.contentClassified(generatorContext.getVariables())
      const actual = contentCatalog
        .findBy({ component: 'the-component', family: 'alias' })
        .filter(({ rel }) => rel.src.version === '4.2.3')
      const expected = ['/the-component/4', '/the-component/4.2', '/the-component/4.2.3']
      expect(actual.map(({ pub }) => pub.url)).to.have.members(expected)
      expect([...new Set(actual.map(({ rel }) => rel.pub.url))]).to.eql(['/the-component/latest'])
      expect(removeFile).to.not.have.been.called()
    })

    it('should not add splat alias if actual version matches active version', () => {
      const contentAggregate = [{ name: 'the-component', version: '4.2' }]
      const latestPerReleaseLineByComponent = new Map([
        ['the-component', new Map([['4.2', { level: 'minor', version: '4.2' }]])],
      ])
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      const contentCatalog = createContentCatalog(contentAggregate, undefined, true)
      generatorContext.updateVariables({ contentCatalog, latestPerReleaseLineByComponent })
      generatorContext.contentClassified(generatorContext.getVariables())
      expect(contentCatalog.findBy({ page: 'alias' })).to.be.empty()
    })
  })

  describe('integration', () => {
    const runGeneratorThroughContentClassified = (contentAggregate) => {
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ contentCatalog: createContentCatalog(contentAggregate, undefined, false) })
      generatorContext.componentsRegistered(generatorContext.getVariables())
      generatorContext.getVariables().contentCatalog.$complete()
      generatorContext.contentClassified(generatorContext.getVariables())
      return generatorContext
    }

    it('should add splat aliases for version lines identified in content aggregate', () => {
      const { contentCatalog } = runGeneratorThroughContentClassified([
        { name: 'the-component', version: '4.2.2' },
        { name: 'the-component', version: '4.2.3' },
        { name: 'the-component', version: '3.8.0' },
        { name: 'the-component', version: '4-3-0' },
        { name: 'another-component', version: '1.0.0' },
        { name: 'another-component', version: '1.1.0' },
        { name: 'unversioned-component', version: '' },
      ]).getVariables()
      const actual = contentCatalog
        .findBy({ family: 'alias' })
        .map((splatAlias) => [splatAlias.pub.url, splatAlias.rel.pub.url])
      const expected = [
        ['/another-component/1', '/another-component/1.1'],
        ['/another-component/1.1.0', '/another-component/1.1'],
        ['/another-component/1.0.0', '/another-component/1.0'],
        ['/the-component/4', '/the-component/4.2'],
        ['/the-component/4.2.3', '/the-component/4.2'],
        ['/the-component/3', '/the-component/3.8'],
        ['/the-component/3.8.0', '/the-component/3.8'],
      ]
      expect(actual).to.eql(expected)
    })

    it('should not add splat alias for older versions in release line', () => {
      const { contentCatalog } = runGeneratorThroughContentClassified([
        { name: 'the-component', version: '4.2.3' },
        { name: 'the-component', version: '4.2.2' },
        { name: 'the-component', version: '4.2.1' },
      ]).getVariables()
      const actual = contentCatalog.findBy({ family: 'alias' })
      actual.forEach((splatAlias) => {
        expect(splatAlias.src.version).to.not.equal('4.2.2')
        expect(splatAlias.rel.src.version).to.not.equal('4.2.2')
      })
      expect(actual).to.have.lengthOf(2)
    })
  })
})
