= Antora Release Line Extension
:toc: preamble
:toc-title: Contents
ifdef::env-gitlab[:toclevels: 1]
:url-docs-antora: https://docs.antora.org
:url-docs-antora-extensions: {url-docs-antora}/antora/latest/extend/extensions/
:url-semver: https://semver.org

The Antora Release Line Extension identifies {url-semver}[SemVer] release lines (major, major.minor) and configures URL routes to the latest version in each line.

== Overview

A SemVer release line consists of two series (major and major.minor) as well as an actual version (major.minor.patch).
This extension automatically detects the use of SemVer release lines in versioned documentation components in your site.
It then sets up routes using URL rewrites (i.e., splat aliases) to the latest version in each line from each series.

* major -> major.minor
* major.minor.patch -> major.minor

To support these routes, it changes the version segment in the URL for the actual version to major.minor (without changing the version in the model).

* major.minor.patch -> major.minor

Normally prereleases are ignored.
However, if a release line only consists of prereleases, then the latest prerelease is used as the latest release in that release line.

If the actual version is the latest version of the documentation component, and the latest version segment (e.g., latest) is enabled, the latest version segment will be used as the target instead.

* major -> latest
* major.minor -> latest
* major.minor.patch -> latest

The idea behind this extension is that URLs to major and patch versions get routed (i.e., redirected) to the release line URL, which is the minor version.
This URL manipulation and routing helps funnel users to the latest version and makes URLs more stable.
In that regard, the extension serves a similar purpose as the latest version segment feature of Antora, except it recognizes the latest version in each release line.

=== Example

Let's assume that you have a component with the following actual versions:

* 3.1.0
* 3.0.1
* 3.0.0

This extension will then change the version segment in the URL for the following versions:

* 3.1.0 -> 3.1
* 3.0.1 -> 3.0

It will then set up the following redirects (i.e., splat aliases):

* 3 -> 3.1
* 3.1.0 -> 3.1
* 3.0.1 -> 3.0

== Prerequisites

In order to use this extension, you must be using Node.js 16 and Antora *3.2.0-alpha.2*.
The following instructions assume you've already set up an Antora playbook file (i.e., _antora-playbook.yml_) to build your site.

To learn about using extensions with Antora, see the {url-docs-antora-extensions}[Antora extension documentation].

== Install

Use the following command to install the extension into your Antora playbook project:

[,console]
----
$ npm i @opendevise/antora-release-line-extension
----

== Register

Open your Antora playbook file and register the extension as an entry in the `antora.extensions` key.
If this key doesn't yet exist in your playbook, first create it.

.antora-playbook.yml
[,yaml]
----
antora:
  extensions:
  - '@opendevise/antora-release-line-extension'
----

== Configure

There's no configuration necessary to enable the version segment redirects for SemVer release lines.

ifdef::env-gitlab[]
== References

* {url-semver}[Semantic Versioning]
* https://producingoss.com/en/release-lines.html[Release lines as defined in _Producing OSS_]
* https://nodesource.com/blog/understanding-how-node-js-release-lines-work/[Understanding how Node.js Release Lines Work]
* https://www.oreilly.com/library/view/pattern-oriented-software-architecture/9781119963998/chap23-sec004.html[Release line as defined in _Pattern-Oriented Software Architecture For Dummies_]
* https://www.jenkins.io/download/lts/[Jenkins LTS Release Line]

endif::[]
== Copyright and License

Copyright (C) 2022-present by OpenDevise Inc. and the individual contributors to this project.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
ifdef::env-gitlab[See link:LICENSE[] to find the full license text.]
